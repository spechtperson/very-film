import React from 'react'
import {Link} from 'react-router-dom'

import AboutUs from '../components/AboutUs'
import Members from '../components/Members'
import Allies from '../components/Allies'
import Clients from '../components/Clients'
import Recognition from '../components/Recognition'

export const Dark = () => <div>
  <AboutUs />
  <Members />
  <Allies />
  <section>
    <p className='small'>
      <Link to='/legal'>Legal</Link>
    </p>
  </section>
</div>

export const Light = () => <div>
  <Recognition />
  <Clients />
</div>
