import React from 'react'
import Markdown from 'react-markdown'

import site from '../lib/site'
import disclaimer from '../lib/disclaimer'

const main = `${site.name}<br/>
${site.legalName}<br/>
${site.munich.street}</br>
${site.munich.zip} ${site.munich.name}</br>

${site.berlin.title}<br/>
${site.berlin.street}<br/>
${site.berlin.zip} ${site.berlin.name} 

${site.phone}<br/>
[${site.email}](${site.email})<br/>
Tax ID ${site.taxId}`

const foot = `Design<br/>
[${site.designBy.name}](${site.designBy.url})

Code<br/>
[${site.codeBy.name}](${site.codeBy.url})`

export const Dark = () => <div>
  <Markdown source={main} />
  <Markdown className='small' source={foot} />
</div>

export const Light = () => <div>
  <Markdown className='small' source={disclaimer} />
</div>
