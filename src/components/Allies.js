import React from 'react'
import orderBy from 'lodash/orderBy'
import styled from 'styled-components'

import allies from '../lib/allies'

const Allies = styled.section`
  a, span {
    &:after {
      content: ', '
    }
    &:last-child {
      &:after {
        content: '.'
      }
    }
  }
`

export default () => <Allies>
  <h3>❤️</h3>
  <p>
    {orderBy(allies, [ally => ally.name.toLowerCase()]).map((ally, i) => {
      return ally.url
        ? <a key={i} href={ally.url} target='_blank'>{ally.name}</a>
        : <span key={i}>{ally.name}</span>
    })}
  </p>
  <p>Very Film is contributor to Berlin based Hirn Faust Auge creative collective.</p>
</Allies>
