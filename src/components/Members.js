import React from 'react'
import styled from 'styled-components'

import members from '../lib/members'

const Members = styled.section`
  h3 span {
    text-transform: initial;
  }
  ul {
    list-style: none;
    padding: 0;
  }
`

export default () => <Members>
  <ul>
    {members.map((member, i) => <li key={i}>
      <h3><span>{member.name}</span><br/>{member.role}</h3>
      <p className='small'>{member.text}<br/><a href={`mailto:${member.mail}`}>{member.mail}</a></p>
    </li>
    )}
  </ul>
</Members>
