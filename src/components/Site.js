import React, { Component, PropTypes } from 'react'
import clamp from 'lodash/clamp'
import Measure from 'react-measure'
import styled, { injectGlobal } from 'styled-components'

import Curtains from './Curtains'
import Stage from './Stage'

const woff2 = require('../assets/fonts/font-webfont.woff2')
const woff = require('../assets/fonts/font-webfont.woff')
const ttf = require('../assets/fonts/font-webfont.ttf')
const svg = require('../assets/fonts/font-webfont.svg')

// eslint-disable-next-line
injectGlobal`
  @font-face {
    font-family: 'bureaugrotesque_fivefiveRg';
    src: url(${woff2}) format('woff2'),
         url(${woff}) format('woff'),
         url(${ttf}) format('truetype'),
         url(${svg}#bureaugrotesque_fivefiveRg) format('svg');
    font-weight: normal;
    font-style: normal;
  }
`

const Site = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  font-family: 'bureaugrotesque_fivefiveRg', Arial, sans-serif;
  line-height: 1.26;
  font-weight: 400;
  section {
    margin-bottom: 2.8em;
    &:last-child {
      margin-bottom: 0;
    }
  }
  .bar {
    text-transform: uppercase;
    font-size: 3.4em;
  }
  p, h1, h2, h3, h4, h5, li {
    margin: 0.7em 0;
    font-size: 1em;
  }
  h3 {
    text-transform: uppercase;
    white-space: pre-line;
  }
  strong {
    text-transform: uppercase;
    font-weight: 400;
  }
  p.small, .small p {
    font-size: 0.58em;
    line-height: 1.3;
  }
  .bold {
    font-weight: 800;
  }
  .buttons a, .buttons button, .button {
    display: inline-block;
    margin-right: 0.4em;
  }
  .dev {
    background-color: rgba(0,0,0,0.88);
    position: fixed;
    top: 20px;
    left: 20px;
    padding: 6px 20px;
    border-radius: 4px;
    color: white;
    font-size: 22px;
    font-family: Courier;
    font-weight: bold;
    border: 1px solid white;
  }
  @media screen and (max-aspect-ratio: 1/1) {
    &:after {
      z-index: 1000;
      content: 'Please turn your device.';
      padding: 2rem;
      position: absolute;
      display: block;
      background-color: black;
      top: 0;
      left: 0;
      right: 0;
      bottom : 0;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`

class SiteClass extends Component {

  constructor(props) {
    super(props)
    this.state = {
      barWidth: 200,
      scrollWidth: 440,
      viewHeight: 600,
      isPristine: true,
      isPlaying: false,
      isFinished: false,
      isMeasured: false
    }
  }

  static childContextTypes = {
    barWidth: PropTypes.number,
    scrollWidth: PropTypes.number,
    viewHeight: PropTypes.number,
    isPlaying: PropTypes.bool,
    isPristine: PropTypes.bool,
    isMeasured: PropTypes.bool,
    isFinished: PropTypes.bool
  }

  getChildContext () {
    return this.state
  }

  handleMeasure = (dimensions) => {
    const viewHeight = dimensions.height
    const curtainWidth = Math.ceil(dimensions.width / 2)
    const barWidth = clamp(Math.floor(curtainWidth * 0.2), 60, 200)
    const scrollWidth = curtainWidth - barWidth
    this.setState({
      isMeasured: true,
      barWidth,
      scrollWidth,
      viewHeight
    })
  }

  handlePlaybackChange = (payload) => {
    this.setState(payload)
  }

  render () {
    const { isFinished, isPlaying, barWidth, isPristine, isMeasured } = this.state
    return <Measure onMeasure={this.handleMeasure}>
      <Site style={{fontSize: Math.round(clamp(barWidth, 0, 400) * 0.21), opacity: isMeasured ? 1 : 0}}>
        {this.props.children}
        <Stage onPlaybackChange={this.handlePlaybackChange} {...{isPlaying, isFinished, isPristine}} />
        <Curtains {...{isPlaying}} />
      </Site>
    </Measure>
  }
}

export default SiteClass