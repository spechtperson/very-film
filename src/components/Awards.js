import React from 'react'
import styled from 'styled-components'

import awards from '../lib/awards'
import frame from '../assets/img/award-frame.png'

const Awards = styled.section`
  ul {
    position: relative;
    list-style: none;
    padding: 0;
    margin: 0;
    width: 100%;
    margin-left: -0.6em;
    width: calc(100% + 1.2em);
    li {
      position: relative;
      display: inline-block;
      display: inline-flex;
      flex-direction: row;
      flex-wrap: nowrap;
      align-items: center;
      margin: 0.3em 0.4em;
      &:before, &:after {
        content: '';
        display: inline-block;
        width: 1.7em;
        height: 3.26em;
        flex-shrink: 0;
        flex-grow: 0;
        background-image: url(${frame});
        background-size: 100% auto;
      }
      &:after {
        transform: scaleX(-1);
      }
      p {
        width: 11em;
        text-align: center;
        font-size: 0.56em;
      }
    }
  }
`

export default () => <Awards>
  <h3 className='default'>Awards</h3>
  <ul className='large'>
    {awards.map((award, i) => {
      return <li key={i}>
        <p>{award.title}, {award.context} ({award.year})</p>
      </li>
    })}
  </ul>
</Awards>
