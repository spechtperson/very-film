import React from 'react'
import styled from 'styled-components'

import recognitions from '../lib/recognitions'

const Recognition = styled.section`
  ul {
    list-style: none;
    padding: 0;
    margin: 0;
    li {
      display: flex;
      flex-direction: column;
    }
    h4 {
      margin: 0;
    }
    p {
      margin: 0.2em 0;
    }
  }
`

export default () => <Recognition>
  <h3>Recognition</h3>
  <ul>
    {recognitions.map((recognition, i) => {
      return <li key={i}>
        <h4>{recognition.context}, {recognition.location} {recognition.date}</h4>
        <p className='small'>{recognition.category}</p>
      </li>
    })}
  </ul>
</Recognition>