import React from 'react'
import Markdown from 'react-markdown'

import about from '../lib/about'

export default () => <section>
  <Markdown source={about.address} />
  <Markdown className='buttons' source={about.text} />
</section>
