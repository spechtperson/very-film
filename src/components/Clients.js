import React from 'react'
import styled from 'styled-components'

import clients from '../lib/clients'

const Clients = styled.section`
  ul {
    list-style: none;
    padding: 0;
    margin: 1em 0;
    li {
      display: inline;
      &:after {
        content: ', ';
      }
      &:last-child {
        &:after {
          content: '';
          display: none;
        }
      }
    }
  }
`

export default () => <Clients>
  <h3>Clients</h3>
  <ul >
    {clients.map((client, i) => {
      return <li key={i}>{client}</li>
    })}
  </ul>
</Clients>
