import React, { Component, PropTypes } from 'react'
import styled from 'styled-components'
import throttle from 'lodash/throttle'
import clamp from 'lodash/clamp'
import find from 'lodash/find'
import { Motion, spring } from 'react-motion'
import { withRouter } from 'react-router-dom'

import Curtain from './Curtain'

import UiEmitter from '../lib/UiEmitter'
import routes from '../lib/routes'
import { absoluteFull } from '../lib/styles'
const sides = ['dark', 'light']

const Wrap = styled.div`
  ${absoluteFull};
  pointer-events: ${props => props.isDragging ? 'auto' : 'none'}
`

const Animate = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  height: 100%;
  width: 50%;
  overflow: hidden;
  ${props => props.side === 'dark' && `
    left: 0;
    right: auto;
    background-color: black;
    color: white;
  `}
  ${props => props.side === 'light' && `
    left: auto;
    right: 0;
    background-color: white;
    color: black;
  `}
`

class Curtains extends Component {

  constructor(props) {
    super(props)
    this.loadingListener = null
    this.state = {
      isPressed: false,
      isLoading: props.location.pathname === '/',
      isOpen: props.location.pathname === '/',
      delta: 0,
      navigateOnRest: false,
      gestureOrigin: [0, 0]
    }
    this.handleMoveThrottled = throttle(this.handleMove, 40)
  }

  static contextTypes = {
    scrollWidth: PropTypes.number,
    barWidth: PropTypes.number,
    to: PropTypes.string,
    bar: PropTypes.string,
    isPlaying: PropTypes.bool,
    viewHeight: PropTypes.number
  }

  getRoute = (path) => {
    return find(routes, {path: path || this.props.location.pathname}) || find(routes, {id: '404'})
  }

  componentWillMount() {
    const route = this.getRoute()
    const nextRoute = find(routes, {path: route.bar.to})
    this.setState({route, nextRoute})
  }

  componentDidMount() {
    if (this.state.isLoading) {
      this.loadingListener = UiEmitter.addListener('isLoaded', () => {
        this.setState({isLoading: false})
        this.loadingListener.remove()
      })
    }
  }

  handleMouseUp = () => {
    const {isPressed, delta, navigateOnRest} = this.state
    if (!isPressed) return
    const tolerance = Math.round(this.context.barWidth * 1.6)
    if (Math.abs(delta) > tolerance && !navigateOnRest) {
      this.setState({
        isPressed: false,
        isDragging: false,
        delta: 0,
        navigateOnRest: true
      })
    } else {
      this.setState({
        isPressed: false,
        isDragging: false,
        navigateOnRest: false,
        delta: 0
      })
    }
  }

  handleMouseDown = ({pageX, pageY}, side) => {
    this.setState({
      isPressed: side,
      delta: 0,
      gestureOrigin: [pageX, pageY]
    })
  }

  handleMove = ({pageX, pageY}) => {
    const {scrollWidth} = this.context
    const {isPressed, gestureOrigin, isOpen} = this.state
    if (!isPressed) return
    const drag = pageX - gestureOrigin[0]
    let delta
    if ((isPressed === 'dark' && isOpen) || (isPressed === 'light' && !isOpen)) {
      delta = clamp(drag, 0, scrollWidth)
    } else {
      delta = clamp(drag, -scrollWidth, 0)
    }
    delta = Math.abs(delta)
    this.setState({delta, isMoving: true, isDragging: true})
  }

  handleMouseMove = ({pageX, pageY}) => {
    this.handleMoveThrottled({pageX, pageY})
  }

  handleTouchStart = (e, side) => {
    this.handleMouseDown(e.touches[0], side)
  }

  handleTouchMove = (e) => {
    const {pageX, pageY} = e.touches[0]
    this.handleMouseMove({pageX, pageY})
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.location !== nextProps.location) {
      const route = this.getRoute(nextProps.location.pathname)
      const nextRoute = find(routes, {path: route.bar.to})
      this.setState({
        route,
        nextRoute,
        isOpen: route.id === 'home'
      })
    }
  }

  handleRest = () => {
    const {nextRoute, navigateOnRest, isPressed} = this.state
    const path = nextRoute.path
    if (navigateOnRest) {
      this.setState({
        navigateOnRest: false,
        isMoving: false
      }, () => this.props.history.push(path))
    } else {
      if (!isPressed) {
        this.setState({
          isMoving: false
        })
      }
    }
  }

  render () {
    const {delta, route, nextRoute, isOpen, navigateOnRest, isMoving, isDragging, isLoading} = this.state
    const {scrollWidth, barWidth, isPlaying} = this.context
    const shift = ((isOpen && !isLoading) ? scrollWidth : 0) + (isPlaying ? barWidth : 0) + ((isOpen && !isLoading) ? -delta : delta) + (navigateOnRest ? ((isOpen && !isLoading) ? -scrollWidth : scrollWidth) : 0)
    const defaultStyle = {
      x: shift
    }
    const styles = {
      x: spring(shift, {
        stiffness: isDragging ? 320 : 300,
        damping: isDragging ? 22 : 34
      })
    }

    const showPreview = (isDragging && delta > barWidth * 1.6) || navigateOnRest

    return <Wrap
      className={`${isOpen ? 'isOpen' : ''}`}
      {...{isDragging}}
      onMouseMove={this.handleMouseMove}
      onTouchMove={this.handleTouchMove}
      onMouseUp={this.handleMouseUp}
      onTouchEnd={this.handleMouseUp}>
      {this.props.children}
      <Motion
        {...{defaultStyle}}
        onRest={this.handleRest}
        style={styles}>
        {({x}) => <Wrap {...{isDragging}}>
          {sides.map((side, i) => <Animate
            key={side}
            side={side}
            style={{
              WebkitTransform: `translate3d(${side === 'light' ? x : -x}px, 0px, 0px)`,
              transform: `translate3d(${side === 'light' ? x : -x}px, 0px, 0px)`
            }}>
            <Curtain
              onMouseDown={(e) => this.handleMouseDown(e, side)}
              onTouchStart={(e) => this.handleTouchStart(e, side)}
              {...{side, route, nextRoute, showPreview, isOpen, isMoving, isDragging, isLoading}}
            />
          </Animate> )}
        </Wrap> }
      </Motion>
      {/*<span className='dev'>
        isLoading: {this.state.isLoading ? 'true' : 'false'}<br/>
        isMoving: {this.state.isMoving ? 'true' : 'false'}<br/>
        isPressed: {this.state.isPressed ? 'true' : 'false'}<br/>
        isDragging: {this.state.isDragging ? 'true' : 'false'}
      </span>*/}
    </Wrap>
  }
}

export default withRouter(Curtains)
