import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { absoluteFull } from '../lib/styles'

const bar = 4.6
const extra = 2
const vertical = 1.1
const horizontal = 1
const offset = 0.26

const CurtainWrap = styled.div`
  ${absoluteFull};
  pointer-events: ${props => props.isDragging ? 'none' : 'auto'};
`

const Bar = styled.div`
  position: absolute;
  width: ${bar}em;
  top: 0;
  bottom: 0;
  user-select: none;
  pointer-events: none;
  span {
    display: inline-block;
    position: absolute;
    top: 50%;
    left: 50%;
    pointer-events: ${props => props.isDragging ? 'none' : 'auto'};
  }
  a {
    display: inline-block;
    cursor: pointer;
    z-index: 200;
  }
  ${props => props.side === 'dark' && `
    left: auto;
    right: 0;
    span {
      transform: translate3d(-50%, -50%, 0px) rotateZ(-90deg);
    }
  `}
  ${props => props.side === 'light' && `
    left: 0;
    right: auto;
    span {
      transform: translate3d(-50%, -50%, 0px) rotateZ(90deg);
    }
  `}
`

const BarWrap = styled.div`
  ${absoluteFull};
  transition-property: transform;
  transition-duration: 0.4s;
  transition-delay: 0.2s;
  transition-timing-function: ease-in-out;
`

const TopBar = styled(BarWrap)`
  transform: translateY(${props => (props.isLoading ? 100 : props.isOpen ? props.showPreview ? -100 : 0 : props.showPreview ? 0 : -100) * (props.side === 'dark' ? -1 : 1)}%);
`

const BottomBar = styled(BarWrap)`
  transform: translateY(${props => (props.isOpen ? props.showPreview ? 0 : 100 : props.showPreview ? 100 : 0) * (props.side === 'dark' ? -1 : 1)}%);
`

const Scroll = styled.div`
  position: relative;
  width: calc(100% + ${extra}em);
  height: 100%;
  overflow-y: scroll;
  -webkit-overflow-scrolling: touch;
  padding-top: ${vertical}em;
  padding-bottom: ${vertical * 1.2}em;
  padding-right: ${horizontal}em;
  padding-left: ${horizontal}em;
  ${props => props.side === 'dark' && `
    padding-left: ${horizontal}em;
    padding-right: ${bar + extra + offset}em;
    a {
      &:hover {
        background-color: white;
        color: black;
      }
    }
  `}
  ${props => props.side === 'light' && `
    padding-left: ${bar + (offset * 1.4)}em;
    padding-right: ${extra + horizontal}em;
    a {
      &:hover {
        background-color: black;
        color: white;
      }
    }
  `}
`

export default class Curtain extends Component {

  shouldComponentUpdate(nextProps, nextState) {
    const {route, showPreview, isOpen, isMoving, isDragging, isLoading} = this.props
    if (
      route.id !== nextProps.route.id ||
      showPreview !== nextProps.showPreview ||
      isMoving !== nextProps.isMoving ||
      isDragging !== nextProps.isDragging ||
      isLoading !== nextProps.isLoading ||
      isOpen !== nextProps.isOpen
    ) {
      return true
    }
    return false
  }

  render () {
    const {side, route, nextRoute, showPreview, isOpen, isMoving, isDragging, isLoading} = this.props
    return <CurtainWrap {...{isDragging}}>
      <Scroll {...{side}}>
        {(isMoving && isOpen) ? nextRoute[side] : route[side]}
      </Scroll>
      <Bar {...{side, isDragging}}>
        <TopBar {...{showPreview, isOpen, side, isLoading}}>
          <span>
            <Link
              className='bar'
              onMouseDown={this.props.onMouseDown}
              onTouchStart={this.props.onTouchStart}
              onDragStart={(e) => e.preventDefault()}
              to='/about'>
              {side === 'dark' ? 'Very' : 'Film'}
            </Link>
          </span>
        </TopBar>
        <BottomBar {...{showPreview, isOpen, side}}>
          <span>
            <Link
              className='bar'
              onMouseDown={this.props.onMouseDown}
              onTouchStart={this.props.onTouchStart}
              onDragStart={(e) => e.preventDefault()}
              to={route.bar.to}>
              {isOpen ? nextRoute.bar[side] : route.bar[side]}
            </Link>
          </span>
        </BottomBar>
      </Bar>
    </CurtainWrap>
  }
}
