import React, { Component } from 'react'
import styled from 'styled-components'
import UiEmitter from '../lib/UiEmitter'
import { absoluteFull } from '../lib/styles'

const words = [
  'Film',
  'Naughty',
  'Munich',
  'Berlin',
  'Tasty',
  'Great',
  'Sexy',
  'Clean',
  'Dirty',
  'Nice'
]

const Loader = styled.div`
  ${absoluteFull};
  display: flex;
  justify-content: center;
  align-items: center;
`

export const DarkLoader = () => <Loader>
  <span className='bar'>Very</span>
</Loader>

export class LightLoader extends Component {

  listener = null
  timeout = null
  interval = null
  state = {
    current: 0,
    time: 1000 / words.length
  }

  startTimer = () => {
    this.interval = setInterval(() => {
      if (this.state.current < words.length - 1) {
        const current = this.state.current + 1
        this.setState({current})
        return
      }
      clearInterval(this.interval)
      this.interval = null
      this.timeout = setTimeout(() => {
        UiEmitter.emit('isLoaded')
        clearTimeout(this.timeout)
        this.timeout = null
      }, 350)
    }, this.state.time)
  }

  componentWillMount() {
    this.listener = UiEmitter.addListener('isReady', () => {
      this.timeout = setTimeout(this.startTimer, 250)
    })
  }

  render () {
    return <Loader>
      <span className='bar'>{words[this.state.current]}</span>
    </Loader>
  }
}
