import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Player from '@vimeo/player'
import styled from 'styled-components'

import UiEmitter from '../lib/UiEmitter'

const Wrap = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  overflow: hidden;
  background-color: black;
  iframe {
    position: relative;
    border: 0;
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    border: 0;
    cursor: pointer;
  }
`

class Stage extends Component {

  handlePlay = () => {
    this.props.onPlaybackChange({isPlaying: true, isPristine: false, isFinished: false})
  }

  handlePause = () => {
    this.player.getCurrentTime().then((seconds) => {
      this.props.onPlaybackChange({isPlaying: false})
    })
  }

  handleEnded = () => {
    this.props.onPlaybackChange({isPlaying: false, isPristine: true, isFinished: true})
  }

  initPlayer = () => {
    this.player = new Player('showreelPlayer')
    // this.player.setVolume(0)
    this.player.on('play', this.handlePlay)
    this.player.on('pause', this.handlePause)
    this.player.on('ended', this.handleEnded)
    this.player.ready().then(() => UiEmitter.emit('isReady'))
  }

  componentDidMount () {
    this.initPlayer()
  }

  componentWillUnmount () {
    this.player.off('play', this.handlePlay)
    this.player.off('pause', this.handlePause)
    this.player.off('ended', this.handleEnded)
  }

  clickOverlay = () => {
    this.player.getPaused().then((paused) => {
      if (paused) {
        this.player.play()
      } else {
        this.player.pause()
      }
    })
  }

  componentWillUpdate(nextProps, nextState) {
    if (!this.player) return
    this.player.getPaused().then((paused) => {
      if (!paused && nextProps.location.pathname !== '/') {
        this.player.pause()
      }
    })
  }

  render () {
    return <Wrap>
      <iframe
        src='https://player.vimeo.com/video/208057127'
        width={640}
        height={360}
        frameBorder={0}
        allowFullScreen
        id='showreelPlayer'
      />
    </Wrap>
  }
}

export default withRouter(Stage)