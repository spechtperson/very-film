import site from './site'

export default {
  address: `[${site.email}](mailto:${site.email})<br/>${site.phone}`,
  text: `As a full service film production company, our heart beats at twenty four to a thousand frames per second. We challenge the silence. With blood, sweat and tears. For this is film. And we worship the power of moving images. Very much.


Got a project in mind?</br>Say [${site.email}](mailto:${site.email})

[VIMEO](http://vimeo.com/veryfilm)
[INSTAGRAM](http://instagram.com/very.film)
[FACEBOOK](http://facebook.com/veryfilm)`
}
