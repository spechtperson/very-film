export default [{
  context: `10th Lago Film Fest`,
  category: `Official Selection`,
  location: `Italy`,
  date: '2014'
}, {
  context: `13th Oldenburger Kurzfilmtage`,
  category: `Official Selection “Midnight Trash”`,
  location: `Germany`,
  date: '2013'
}, {
  context: `43rd Alcine – Festival De Cine De Alcala De Henares`,
  category: `ShortLatino Video Library Catalogue`,
  location: `Spain`,
  date: '2013'
}, {
  context: `Portobello Film Festival London`,
  category: `Official Selection “German Films”`,
  location: `United Kingdom`,
  date: '2013'
}, {
  context: `37th Open Air Filmfest Weiterstadt`,
  category: `Official Selection “Competition”`,
  location: `Germany`,
  date: '2013'
}, {
  context: `29th International Short Film Festival Hamburg`,
  category: `Official Selection “German Competition”`,
  location: `Germany`,
  date: '2013'
}, {
  context: `66th Cannes International Film Festival`,
  category: `SHORT FILM CORNER Market Screening`,
  location: `France`,
  date: '2013'
}, {
  context: `42nd Sehsüchte Filmfestival`,
  category: `Official Selection “Excess”`,
  location: `Germany`,
  date: '2013'
}, {
  context: `I’ve Seen Films International Film Festival`,
  category: `Official Selection`,
  location: `Italy`,
  date: '2010'
}, {
  context: `Budapest Shortfilm Festival`,
  category: `Official Selection`,
  location: `Hungary`,
  date: '2010'
}, {
  context: `Bornshorts International Film Festival`,
  category: `Official Selection`,
  location: `Denmark`,
  date: '2010'
}, {
  context: `Cinefiesta Puerto Rico Short Filmfestival`,
  category: `Official Selection`,
  location: `USA`,
  date: '2010'
}, {
  context: `Sardinia Film Festival`,
  category: `Official Selection`,
  location: `Italy`,
  date: '2010'
}, {
  context: `Hamilton Music and Film Festival`,
  category: `Official Selection`,
  location: `Canada`,
  date: '2010'
}, {
  context: `20min|max Short Film Festival`,
  category: `Official Selection`,
  location: `Germany`,
  date: '2010'
}, {
  context: `Backup Filmfestival`,
  category: `Official Selection`,
  location: `Germany`,
  date: '2010'
}, {
  context: `39th Sehsüchte Filmfestival`,
  category: `Start-Up-Award`,
  location: `Germany`,
  date: '2010'
}, {
  context: `European Media Art Festival`,
  category: `Official Selection`,
  location: `Germany`,
  date: '2010'
}, {
  context: `MIT European Short Film Festival`,
  category: `Official Selection`,
  location: `United States of America`,
  date: '2010'
}, {
  context: `Shortshots Berlin`,
  category: `Official Selection “MIXTAPE”`,
  location: `Germany`,
  date: '2010'
}, {
  context: `International Filmfestival Rotterdam`,
  category: `Official Selection “NPS New Arrivals”`,
  location: `Netherlands`,
  date: '2010'
}, {
  context: `Bamberger Kurzfilmtage`,
  category: `Official Selection`,
  location: `Germany`,
  date: '2010'
}, {
  context: `La.Meko Short Film Festival Landau`,
  category: `Best Clip Award`,
  location: `Germany`,
  date: '2009'
}, {
  context: `International Short Film Festival Berlin`,
  category: `Official Selection “viral video award”`,
  location: `Germany`,
  date: '2009'
}, {
  context: `Kitaso Filmtage Erlangen`,
  category: `2nd Audience Choice Award`,
  location: `Germany`,
  date: '2009'
}]
