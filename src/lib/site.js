export default {
  name: 'Very Film',
  phone: `+49 (0) 178 1490060`,
  legalName: `Obermeier+Strehlow GbR`,
  taxId: `DE296378552`,
  email: `hello@very.film`,
  berlin: {
    title: `Office Berlin`,
    name: `Berlin`,
    street: `Adalbertstr. 86`,
    zip: 10997
  },
  munich: {
    title: `Office Munich`,
    name: `Munich`,
    street: `Humboldtstr. 15`,
    zip: 81543,
    phone: `+49 (0) 160 5572093`
  },
  designBy: {
    name: `Any Studio`,
    url: `http://any.studio`
  },
  codeBy: {
    name: `@spechtperson`,
    url: `http://hirnfaustauge.de`
  }
}
