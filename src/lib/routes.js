import React from 'react'
import { Dark as AboutDark, Light as AboutLight } from '../views/About'
import { Dark as LegalDark, Light as LegalLight } from '../views/Legal'
import { DarkLoader, LightLoader } from '../components/Loader'

export default [{
  id: 'home',
  title: 'Showreel',
  path: '/',
  exact: true,
  dark: <DarkLoader />,
  light: <LightLoader />,
  bar: {
    dark: 'Very',
    light: 'Film',
    to: '/about'
  }
}, {
  id: 'about',
  title: 'About',
  path: '/about',
  exact: true,
  dark: <AboutDark />,
  light: <AboutLight />,
  bar: {
    dark: 'Show',
    light: 'Reel',
    to: '/'
  }
}, {
  id: 'legal',
  title: 'Legal',
  path: '/legal',
  exact: true,
  dark: <LegalDark />,
  light: <LegalLight />,
  bar: {
    dark: 'Very',
    light: 'Legal',
    to: '/'
  }
}, {
  id: '404',
  title: '404',
  path: '*',
  exact: false,
  dark: null,
  light: null,
  bar: {
    dark: 'Very',
    light: 'Sorry',
    to: '/'
  }
}]
