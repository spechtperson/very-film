import {css} from 'styled-components'

export const sizes = {
  handheld: 400,
  phablet: 550,
  tablet: 768,
  medium: 1020,
  laptop: 1200,
  desktop: 1400,
  huge: 1680
}

export function minWidth(px) {
  // use em in breakpoints to work properly cross-browser and support users
  // changing their browsers font-size: https://zellwk.com/blog/media-query-units/
  const emSize = px / 16
  return 'min-width: ' + emSize + 'em'
}

export const media = Object.keys(sizes).reduce((accumulator, label) => {
  accumulator[label] = (...args) => css`
    @media (${minWidth(sizes[label])}) {
      ${css(...args)}
    }
  `
  return accumulator
}, {})

export const absoluteFull = `
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  overflow: hidden;
`
