export default [{
  title: `Start-Up-Award`,
  context: `39th SEHSÜCHTE FILMFESTIVAL`,
  year: 2010
}, {
  title: `Best Clip`,
  context: `LA.MEKO SHORT FILM FESTIVAL LANDAU`,
  year: 2009
}, {
  title: `2nd Audience Choice`,
  context: `KITASO FILMTAGE ERLANGEN`,
  year: 2009
}]
