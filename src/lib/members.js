export default [{
  name: `Anton Zabriskie`,
  role: `Director`,
  mail: `anton@very.film`,
  text: `While studying philosophy and sociology at the Friedrich-Alexander University of Erlangen, Anton spent most of his time assisting on photography and film productions. Inspired and entirely hooked, he started doing commercials and graduated in design at the Georg-Simon-Ohm University of Applied Sciences Nuremberg in 2012. Having a serious addiction ever since, he also works as an Editor. Anton is partner of Very Film and lives in Berlin.`
}, {
  name: `Matthias Obermeier`,
  role: `Director of Photography\n& Editor`,
  mail: `matthias@very.film`,
  text: `A 2011 graduate of the Bavarian Academy for Television in cinematography, Matthias brings years of experience in film and television productions across all kinds of commercial, editorial and experimental formats. Obsessed with smooth pans and depths of field, he is all into getting the right imagery work. Matthias is partner of Very Film and takes care in Munich.`
}, {
  name: `Jakob Kornelli`,
  role: `Creative Director`,
  mail: `jakob@very.film`,
  text: `Jakob graduated in design at the Free University of Bolzano, Italy in 2011 and continued his studies at Hasso-Plattner-Institute Potsdam. Being all into brand strategy, design thinking and creative consulting he is founder of the Hirn Faust Auge creative network and Art Director of Stadtaspekte magazine. Together with Max Edelberg, Jakob runs the design office Any Studio in Berlin.`
}]
