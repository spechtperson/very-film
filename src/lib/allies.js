export default [{
  name: `@spechtperson`,
  url: `http://www.hirnfaustauge.de`
}, {
  name: `Any Studio`,
  url: `http://www.any.studio`
}, {
  name: `aug&ohr medien`,
  url: `http://www.augohr.de/`
}, {
  name: `BFS Entertainment`,
  url: `http://www.bfsentertainment.de/`
}, {
  name: `Martin Blumöhr`,
  url: `http://www.martin-blumoehr.de/`
}, {
  name: `Felix Boekamp`,
  url: `http://www.arsdilettanti.de/content/felix-boekamp`
}, {
  name: `Jochen Brimmer`,
  url: `http://www.jochen-brimmer.de/`
}, {
  name: `Chaussee Soundvision`,
  url: `http://chaussee-soundvision.com/`
}, {
  name: `christmasisdead`,
  url: `http://www.christmasisdead.com/`
}, {
  name: `Christopher Donaghue`,
  url: `http://christopherdonaghue.weebly.com/`
}, {
  name: `Björn Dunne`,
  url: `http://www.behance.net/bjoerndunne`
}, {
  name: `Fabian Filß`,
  url: `http://dasauge.de/-nomadicdesign/`
}, {
  name: `Kai von Glasow`,
  url: `http://kaivonglasow.com/`
}, {
  name: `Pablo Gotzes`,
  url: `http://www.imdb.com/name/nm4171597/`
}, {
  name: `graupause`,
  url: `http://www.graupause.com/`
}, {
  name: `Christopher Hegenberg`,
  url: `http://christopher-hegenberg.com/`
}, {
  name: `heyblend`,
  url: `http://www.heyblend.com/`
}, {
  name: `Instant Waves`,
  url: `http://www.instantwaves.com/`
}, {
  name: `Florian Leitl`,
  url: `http://www.millahn.de/editoren/`
}, {
  name: `Andi Mayr`,
  url: `http://www.andimayr.de/`
}, {
  name: `Anna Miholic`,
}, {
  name: `Mind and Image`,
  url: `http://mi-mc.com/`
}, {
  name: `David Olbrich`,
}, {
  name: `SoftlifeMedia`,
  url: `http://www.softlifemedia.de/`
}, {
  name: `Sound Shop`,
  url: `http://www.soundshopmix.de/`
}, {
  name: `greatfull design`,
  url: `http://www.greatfulldesign.com/`
}, {
  name: `Simon Toplak`,
  url: `http://www.simontoplak.com/`
}, {
  name: `unfun`,
  url: `http://unfun.de/`
}, {
  name: `Alex Vexler`,
  url: `http://www.alexandervexler.com/`
}, {
  name: `Steve Wühr`,
  url: `http://vimeo.com/user2578120`
}, {
  name: `Joni Zaza`,
  url: `http://www.jonizaza.de/`
}]