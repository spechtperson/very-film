import React, { Component } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Helmet from 'react-helmet'

import Site from './components/Site'

import site from './lib/site'

export default class App extends Component {
  render () {
    return (
      <Router>
        <Site>
          <Helmet titleTemplate={`%s - ${site.name}`} />
        </Site>
      </Router>
    )
  }
}
