#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

# eliminate render blocking js
replace-in-file \
  '<script type="text/javascript" src="/static/js/' \
  '<script type="text/javascript" async defer src="/static/js/' \
  build/{index,reset}.html \
  --verbose

replace-in-file \
  '<script type="text/javascript" src="/static/js/' \
  '<script type="text/javascript" async defer src="/static/js/' \
  build/**/*.html \
  --verbose
